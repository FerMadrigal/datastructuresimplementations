package mx.edu.utr.datastructures.st1469;

import mx.edu.utr.datastructures.List;

/**
 *
 * @author Fer Madrigal
 */
public class LinkedList implements List {

    class Node {

        Object element;
        Node previous;
        Node next;

        public Node(Object element, Node previous, Node next) {
            this.element = element;
            this.previous = previous;
            this.next = next;
        }

    }
    int size;
    Node head;
    Node tail;

    public LinkedList() {
        this.head = new Node(null, null, null);
        this.tail = new Node(null, this.head, null);
        this.head.next = this.tail;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean add(Object element) {
        addBefore(element, tail);
        return true;

    }

    private void addBefore(Object element, Node tail) {

        Node nod = new Node(element, tail.previous, tail.next);
        nod.previous.next = nod;
        nod.next.previous = nod;
        size++;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void add(int index, Object element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object get(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object remove(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object set(int index, Object element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int size() {
        return size;
    }

}
