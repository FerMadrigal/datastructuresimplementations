package mx.edu.utr.datastructures.st1469;

import java.util.Arrays;
import mx.edu.utr.datastructures.*;

/**
 *
 * @author Fer Madrigal
 */
public class ArrayList implements List {
    /**
     * Declaration of objects
     */
    public Object[] elements;
    public int size;

    /**
     * 
     * @param initialCapacity 
     */
    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }
     /**
      * It is te value of Initial Capacity
      * in this case the initial vaule is 10
      */
    public ArrayList() {
        this(10);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean add(Object element) {
        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void add(int index, Object element) {
        rangeCheckAdd(index);
        ensureCapacity(size + 1);
        for (int i = size; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void clear() {

        for (int i = 0; i < elements.length; i++) {
            elements[i] = null;
            size = 0;
        }

    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object get(int index) {
        rangeCheckAdd(index);
        for (int i = 0; i <= size; i++) {
            if (index == i - 1) {
                elements[index] = index;
            }

        }
        return elements[index];

    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int indexOf(Object element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])) {
                    return i;
                }

            }

        }
        throw new IndexOutOfBoundsException("That element doesn't exist");
    }

    /**
     * {@inheritDoc }
     */
    @Override //
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object remove(int index
    ) {
        rangeCheckAdd(index);
        Object old = elements[index];
        int indexMoved = size - index - 1;
        if (indexMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, indexMoved);
        }
        elements[--size] = null;
        return old;
    }

    /**
     * {@inheritDoc }
     */
    @Override //
    public Object set(int index, Object element) {
        rangeCheckAdd(index);
        Object old = elements[index];
        elements[index] = element;
        return old;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * In this method we save the duplicate old capacity as new capacity of the
     * new array. The "copy of "allows to save the elements of the set that it
     * had and adds the new capacity of the array It will have the ability to
     * duplicate when it will be exceeded
     *
     * @param minCapacity
     */
    public void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            Object old[] = elements;
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }

    }

    /**
     * This method allows to print all the Array's elements
     */
    public void Print() {
        for (int i = 0; i < elements.length; i++) {
            System.out.println(elements[i]);
        }
    }

    /**
     * This method validates that the index is not greater than its capacity or
     * less than 0
     *
     * @param index
     */
    private void rangeCheckAdd(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Out of bound");

        }
    }
}
